# README #

This is my implementation of dependency injection in PHP using reflection from type hinting in methods.

### What is this repository for? ###

* Understantind autoloading and [DI](https://en.wikipedia.org/wiki/Dependency_injection) in PHP
* version - x

### How do I get set up? ###

* Requires php 5.3 or higher
* git clone https://corsa1r@bitbucket.org/corsa1r/dependency-injection-prototype.git