<?php

use core\http\router\Router;

Router::get('/', 'HomeControllers@index');
Router::get('/user/{user}/profile/{route}', 'HomeController@user');