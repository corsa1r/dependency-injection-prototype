<?php

include './core/Autoloader.php';
Core\Autoloader::registerAutoload();

$requestHandler = new Core\Http\Request\RequestHandler();
$request = $requestHandler->handleRequest();

require './app/config/routes.php';

$application = new Core\Application;
$boot = $application->bootstrap($request);
$application->run($boot);