<?php

namespace Core;

class Autoloader {

    /**
     * This method registers autoload
     */
    public static function registerAutoload() {
        spl_autoload_register(__NAMESPACE__ . '\Autoloader::load');
    }

    public static function load($class) {
        $mainDir = __DIR__. DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR;
        $classPath = strtolower(str_replace("\\", DIRECTORY_SEPARATOR, $class));
        
        include_once $mainDir.$classPath. self::$AUTOLOAD_EXTENSION;
    }
    
    private static $AUTOLOAD_EXTENSION = '.php';

}
