<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace core\globals;

/**
 * Description of Server
 *
 * @author venchev
 */
class Server implements \core\interfaces\SetterGetterInterface {

    private $data = null;

    public function __construct() {
        $this->update();
    }

    private function update() {
        $this->data = $_SERVER;
    }

    public function set($key, $value) {
        throw new Exception('This class is read only');
    }

    public function get($key) {
        $this->update();

        if (isset($this->data[$key]) && !empty($this->data)) {
            return $this->data[$key];
        }

        return null;
    }

    public function exists($key) {
        ;
    }

}
