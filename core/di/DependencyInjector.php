<?php

namespace core\di;

class DependencyInjector {

    public function analize($controller, $method, $methodParams) {
        $reflectionMethod = new \ReflectionMethod($controller, $method);

        $params = [];

        foreach ($reflectionMethod->getParameters() as $index => $param) {
            if (!is_null($param->getClass())) {
                $params[] = $this->inject($param, $methodParams[$index]->value);
            } else {
                $params[] = $methodParams[$index]->value;
            }
        }
        
        return $params;
    }

    private function inject($param, $value) {
        $class = $param->getClass()->name;
        return new $class($value);
    }

}
