<?php

namespace Core;

use core\http\router\Router;
use core\http\router\RouteParser;
use core\http\router\Route;
use core\managers\ModelManager;
use core\managers\ControllersManager;
use core\http\router\ParsedRoute;
use core\di\DependencyInjector;
use core\interfaces\RouteInterface;

class Application {

    private $modelManager;
    private $controllerManager;

    public function __construct() {
        $this->modelManager = new ModelManager();
        $this->controllerManager = new ControllersManager();
    }

    public function run(RouteInterface $route) {
        if ($route->status) {
            $output = call_user_method_array($route->method, $route->controller, array_values($route->methodParams));
        } else {
            echo 404;
        }

        echo "<pre>";
        var_dump($route);
        echo "</pre>";

        //set headers for 404
    }

    public function bootstrap(Request $request) {
        $routeParser = new RouteParser();
        $route = $routeParser->parse($request, Router::each());

        if ($route->status) {
            $parsedRoute = $this->completeRoute($route);
            $dependencyInjector = new DependencyInjector();
            $parsedRoute->methodParams = $dependencyInjector->analize($parsedRoute->controller, $parsedRoute->method, $route->getParams());

            return $parsedRoute;
        }

        return $route;
    }

    private function completeRoute(Route $route) {
        if (!$this->controllerManager->exists($route->controller)) {
            throw new \Exception('Cannot find controller '
            . $route->controller
            . ' for route: ' . $route->definition
            );
        }

        $controller = $this->controllerManager->getRef($route->controller);

        if (!method_exists($controller, $route->method)) {

            throw new \Exception("Call of undefined method {$route->method}() in "
            . $route->controller
            . ' for route: ' . $route->definition
            );
        }

        $parsedRoute = new ParsedRoute();
        $parsedRoute->controller = $controller;
        $parsedRoute->method = $route->method;
        $parsedRoute->methodParams = $route->methodParams;

        return $parsedRoute;
    }

}
