<?php

namespace Core\Http\Request;

use core\globals\Server;
use Core\Http\Request\Request;
use \core\http\router\UnparsedRoute;

class RequestHandler {

    private $server;

    public function __construct() {
        $this->server = new Server();
    }

    public function handleRequest() {

        $request = new Request;
        $request->type = $this->determineMethod();
        $request->path = new UnparsedRoute();
        $request->path->pathPattern = $this->getPath();

        return $request;
    }

    private function getPath() {
        $script_name = \str_replace('index.php', '', $this->server->get('SCRIPT_NAME')); //Project dir/index.php
        $path = \str_replace($script_name, '', \urldecode($this->server->get('REQUEST_URI')));
        
        return $path;
    }

    private function determineMethod() {
        $method = $this->server->get('REQUEST_METHOD');

        switch ($method) {
            case RequestHandler::$METHOD_GET :
            case RequestHandler::$METHOD_POST :
            case RequestHandler::$METHOD_PUT:
            case RequestHandler::$METHOD_DELETE :
            case RequestHandler::$METHOD_HEAD :
            case RequestHandler::$METHOD_OPTIONS :
                return $method;
            default:
                return RequestHandler::$METHOD_UNKNOWN;
        }
    }

    /**
     * Different method types
     * @var string
     */
    public static $METHOD_PUT = 'PUT';
    public static $METHOD_POST = 'POST';
    public static $METHOD_GET = 'GET';
    public static $METHOD_HEAD = 'HEAD';
    public static $METHOD_DELETE = 'DELETE';
    public static $METHOD_OPTIONS = 'OPTIONS';
    public static $METHOD_UNKNOWN = 'UNKNOWN';
}
