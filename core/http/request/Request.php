<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Core\Http\Request;

use core\globals\Server;

class Request {

    public $type;
    public $request_time;
    public $path;

    public function __construct() {
        $this->setCreatedAt();
    }

    private function setCreatedAt() {
        $server = new Server();
        $this->request_time = $server->get('REQUEST_TIME');
    }

}
