<?php

namespace core\http\router;

class RouteParam {

    private $type;
    private $binded = false;
    public $value;

    public function setType($type) {
        $this->type = $type;
        $this->binded = is_numeric($this->value);
    }

    public function getType() {
        return $this->type;
    }

    public function isEmpty() {
        return empty($this->value);
    }

    public function isRouteBinded() {
        return $this->binded;
    }

    public function getModelName() {
        if ($this->isRouteBinded()) {
            return ucfirst(strtolower($this->getType()));
        }

        return null;
    }

}
