<?php

namespace core\http\router;

use Core\Http\Request\RequestHandler;
use core\http\router\UnparsedRoute;

class Router {

    private static $methods = [];

    public static function each() {
        return self::$methods;
    }

    private static function register($method, $pathPattern, $controllerPattern) {

        if (is_null($controllerPattern)) {
            throw new \Exception(__CLASS__ . '::' . debug_backtrace()[1]['function'] . '() Invalid controller pattern');
        }

        $unparsedMethod = new UnparsedRoute();
        $unparsedMethod->pathPattern = $pathPattern;
        $unparsedMethod->controllerPattern = $controllerPattern;

        self::$methods[$method][] = $unparsedMethod;
    }

    public static function get($pathPattern, $controller = null) {
        self::register(RequestHandler::$METHOD_GET, $pathPattern, $controller);
    }

    public static function post($pathPattern, $controller = null) {
        self::register(RequestHandler::$METHOD_POST, $pathPattern, $controller);
    }

    public static function put($pathPattern, $controller = null) {
        self::register(RequestHandler::$METHOD_PUT, $pathPattern, $controller);
    }

    public static function delete($pathPattern, $controller = null) {
        self::register(RequestHandler::$METHOD_DELETE, $pathPattern, $controller);
    }

    public static function head($pathPattern, $controller = null) {
        self::register(RequestHandler::$METHOD_HEAD, $pathPattern, $controller);
    }

    public static function options($pathPattern, $controller = null) {
        self::register(RequestHandler::$METHOD_OPTIONS, $pathPattern, $controller);
    }

    public static function unknown($pathPattern, $controller = null) {
        self::register(RequestHandler::$METHOD_UNKNOWN, $pathPattern, $controller);
    }

    private function __construct() {
        //this class cant be instantiated
    }

    public static $HOME = '/';

}
