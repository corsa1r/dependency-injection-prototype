<?php

namespace core\http\router;

class ParsedRoute implements \core\interfaces\RouteInterface {

    public $controller;
    public $method;
    public $methodParams = [];
    public $status = true;

}
