<?php

namespace core\http\router;

/**
 * Description of Route
 *
 * @author venchev
 */
class Route implements \core\interfaces\RouteInterface {

    public $status;
    public $controller;
    public $method;
    public $methodParams = [];
    public $definition;
    public static $STATUS_FOUND = true;
    public static $STATUS_NOT_FOUND = false;

    public function __construct() {
        $this->status = self::$STATUS_NOT_FOUND;
    }

    public function getParams() {
        return array_values($this->methodParams);
    }

    public function getRawParams() {
        $rawParams = [];

        foreach ($this->getParams() as $param) {
            $rawParams[] = $param->value;
        }

        return $rawParams;
    }

}
