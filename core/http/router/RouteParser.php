<?php

namespace core\http\router;

use Core\Http\Request\Request;
use core\http\router\Router;
use core\http\router\RouteParserResult;
use core\http\router\Route;
use core\http\router\RouteParam;
use core\http\router\UnparsedRoute;

class RouteParser {

    /**
     * 
     * @param Request $request
     * @param type $routesPath
     * 
     * @return Route
     */
    public function parse(Request $request, $routesPath) {

        $invalidRoute = new Route();
        $invalidRoute->status = Route::$STATUS_NOT_FOUND;

        if (!isset($routesPath[$request->type])) {
            return $invalidRoute;
        }

        //lookup only for needed requests
        foreach ($routesPath[$request->type] as $path) {
            $need = $this->parsePath($request->path);
            $exists = $this->parsePath($path);

            if (count($need) != count($exists)) {
                continue;
            }


            $route = $this->match($need, $exists);

            if ($route->status == Route::$STATUS_FOUND) {
                $route->controller = $this->getPathController($path);
                $route->method = $this->getPathMethod($path);
                $route->definition = $path->pathPattern;
                return $this->filterRouteParams($route);
            }
        }

        return $invalidRoute;
    }

    private function explodeControllerPattern(UnparsedRoute $route) {
        $explode = explode("@", $route->controllerPattern);

        if (count($explode) != 2) {
            throw new Exception(self::$ERROR_INVALID_CONTROLLER_PATTERN);
        }

        return $explode;
    }

    private function getPathController(UnparsedRoute $route) {
        return array_shift($this->explodeControllerPattern($route));
    }

    private function getPathMethod(UnparsedRoute $route) {
        return array_pop($this->explodeControllerPattern($route));
    }

    private function filterRouteParams(Route $route) {
        $route->methodParams = \array_filter($route->methodParams, function (RouteParam $param) {
            return !$param->isEmpty();
        });

        return $route;
    }

    private function parsePath(UnparsedRoute $path) {
        $parsedPath = array_filter(explode('/', $path->pathPattern));
        array_unshift($parsedPath, Router::$HOME);

        return $parsedPath;
    }

    private function match($need, $exists) {
        $neededMatches = count($need);
        $currentMathes = 0;
        $route = new Route();

        for ($i = 0; $i < $neededMatches; $i++) {
            $parseResult = $this->compare($need[$i], $exists[$i]);
            if ($parseResult->status) {
                $currentMathes += 1;

                $this->fillParam($route, $parseResult->param);
            }
        }

        if ($neededMatches == $currentMathes) {
            $route->status = Route::$STATUS_FOUND;
        }

        return $route;
    }

    private function fillParam(Route $route, RouteParam $param) {
        if ($param) {
            $route->methodParams[] = $param;
        }
    }

    private function compare($partA, $partB) {
        $parserResut = new RouteParserResult();
        $param = new RouteParam();
        $type = $this->getType($partB);

        if (!is_null($type)) {
            $param->value = $partA;
            $param->setType($type);

            $parserResut->status = true;
        } elseif ($partA == $partB) {
            $parserResut->status = true;
        }

        $parserResut->param = $param;

        return $parserResut;
    }

    private function getType($partB) {
        $con1 = (bool) (substr($partB, 0, 1) == self::$EXP_TAG_BEGIN);
        $con2 = (bool) (substr($partB, -1, 1) == self::$EXP_TAG_END);

        if ($con1 && $con2) {
            return substr($partB, 1, -1);
        }

        return null;
    }

    private static $EXP_TAG_BEGIN = '{';
    private static $EXP_TAG_END = '}';
    private static $ERROR_INVALID_CONTROLLER_PATTERN = 'Invalid controll pattern for routes. Example: ControllerName@methodName';

}
