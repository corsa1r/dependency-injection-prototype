<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace core\managers;

/**
 * Description of ControllersManager
 *
 * @author venchev
 */
class ControllersManager extends \core\managers\ManagerManager implements \core\interfaces\ManagerInterface {

    public function __construct() {
        $this->setDefaultPath();
    }

    public function setDefaultPath() {
        $this->path = 'app' . DIRECTORY_SEPARATOR . 'http' . DIRECTORY_SEPARATOR . 'controllers' . DIRECTORY_SEPARATOR;
    }

}
