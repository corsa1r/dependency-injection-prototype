<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace core\managers;

/**
 * Description of ManagerManager
 *
 * @author venchev
 */
class ManagerManager {

    protected $path;

    public function exists($name) {
        return file_exists($this->path . $name . '.php');
    }

    public function getRef($name) {
        $class = $this->getClass($name);
        return new $class();
    }

    public function getClass($name) {
        //echo "Trying to instantiate " . $name;
        $namespace = $this->path;
        $class = $namespace . $name;
        return $class;
    }

}
