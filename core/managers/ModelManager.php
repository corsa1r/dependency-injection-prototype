<?php

namespace core\managers;

class ModelManager extends \core\managers\ManagerManager implements \core\interfaces\ManagerInterface {

    public function setDefaultPath() {
        $this->path = 'app' . DIRECTORY_SEPARATOR . 'models' . DIRECTORY_SEPARATOR;
    }

    public function __construct() {
        $this->setDefaultPath();
    }

}
